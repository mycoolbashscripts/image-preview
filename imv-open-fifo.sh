#!/usr/bin/env bash
#full credit goes to sayanarijit on the xplr discord and matrix servers

FIFO_PATH="$1"
IMAGE="$2"
MAINWINDOW="$(xdotool getactivewindow)"
IMV_PID=$(pgrep imv-x11)

if [ ! "$IMV_PID" ]; then
	imv-x11 "$IMAGE"&
	IMV_PID=$!
fi

sleep 0.5

xdotool windowactivate "$MAINWINDOW"

while read -r path; do
#	imv-msg "$IMV_PID" open "$path"
#	imv-msg "$IMV_PID" close all
	imv-open "$path"
done < "$FIFO_PATH"

imv-msg $IMV_PID quit
[ -e "$FIFO_PATH" ] && rm $FIFO_PATH
