#!/usr/bin/env bash

image=$1
MAINWINDOW="$(xdotool getactivewindow)"
echo $MAINWINDOW 0
pgrep imv-x11
running=$?
if [ $running -eq 1 ]; then
	imv-x11 $image&
	xdotool getactivewindow
	sleep 0.2
	xdotool getactivewindow
	xdotool windowactivate "$MAINWINDOW"
	echo $MAINWINDOW 1
else
	imv_pid=$(pgrep imv-x11)
#	imv-msg $imv_pid close 1
	sleep 0.2
	imv-msg $imv_pid open $image
	sleep 0.2
	imv-msg $imv_pid goto -1
	sleep 0.2
	echo $MAINWINDOW 2
	xdotool windowactivate "$MAINWINDOW"
	xdotool getactivewindow
fi
echo $MAINWINDOW
